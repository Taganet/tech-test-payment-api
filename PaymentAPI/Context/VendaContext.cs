using Microsoft.EntityFrameworkCore;
using PaymentAPI.Models;

namespace PaymentAPI.Context
{
    public class VendaContext : DbContext
    {
        public VendaContext(DbContextOptions<VendaContext> options) : base(options)
        {

        }

        public DbSet<Venda> Vendas { get; set; }
        public DbSet<Vendedor> Vendedores { get; set; }
        public DbSet<ItemVenda> ItensVendas { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Venda>()
            .HasOne(s => s.Vendedor)
            .WithMany(v => v.Vendas)
            .HasForeignKey(s => s.VendedorId)
            .HasPrincipalKey(v => v.Id);
        }
    }
}