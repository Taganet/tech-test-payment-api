using Microsoft.AspNetCore.Mvc;
using PaymentAPI.Models;
using PaymentAPI.Context;

namespace PaymentAPI.Controllers
{
    [ApiController]
    [Route("Vendas")]
    public class VendaController : ControllerBase
    {
        private readonly VendaContext _context;

        public VendaController(VendaContext context)
        {
            _context = context;
        }

        [HttpPost("Vendedor")]
        public IActionResult CadastrarVendedor(Vendedor vendedor)
        {
            _context.Add(vendedor);
            _context.SaveChanges();
            return Ok(vendedor);
        }
        
        [HttpPost("RegistrarVenda")]
        public IActionResult RegistrarVenda(Venda venda)
        {
            _context.Add(venda);
            _context.SaveChanges();

            return CreatedAtAction(nameof(VendaPorId), new { id = venda.Id }, venda);
        }

        [HttpGet("TodasAsVendas")]
        public IActionResult TodasAsVendas()
        {
            var vendas = _context.Vendas;

            return Ok(vendas);
        }

        [HttpGet("PorId/{id}")]
        public IActionResult VendaPorId(int id)
        {
            var venda = _context.Vendas.Find(id);

            if (venda == null)
            {
                return NotFound();
            }
            return Ok(venda);
        }

        [HttpPut("Atualiza{id}")]
        public IActionResult AtualizaStatus(int id, StatusVendaEnum status)
        {
            var vendaBanco = _context.Vendas.Find(id);

            if((vendaBanco.StatusVenda == StatusVendaEnum.AguardandoPagamento) && ((status == StatusVendaEnum.PagamentoAprovado) || (status == StatusVendaEnum.Cancelado) || (status == StatusVendaEnum.AguardandoPagamento)))
                vendaBanco.StatusVenda = status;
            else if((vendaBanco.StatusVenda == StatusVendaEnum.PagamentoAprovado) && ((status == StatusVendaEnum.EnviadoParaTransportadora) || (status == StatusVendaEnum.Cancelado) || (status == StatusVendaEnum.PagamentoAprovado)))
                vendaBanco.StatusVenda = status;
            else if ((vendaBanco.StatusVenda == StatusVendaEnum.EnviadoParaTransportadora) && ((status == StatusVendaEnum.Entregue) || (status == StatusVendaEnum.EnviadoParaTransportadora)))
                vendaBanco.StatusVenda = status;
            else
                return BadRequest(new { Erro = $"Não é permitido passar a venda de status {vendaBanco.StatusVenda} para {Enum.GetName(typeof(StatusVendaEnum), status)}" });

            _context.Vendas.Update(vendaBanco);
            _context.SaveChanges();
            return Ok(vendaBanco);
        }

    }
}