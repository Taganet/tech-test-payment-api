using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PaymentAPI.Models
{
    public class Venda
    {
        public int Id { get; set; }
        public DateTime DataVenda { get; set; }
        public int VendedorId { get; set; }
        public StatusVendaEnum StatusVenda { get; set; }
        public virtual ItemVenda ItensVenda { get; set; }
        public decimal ValorTotalVenda { get; set; }

        public Vendedor Vendedor { get; set; }
    }
}