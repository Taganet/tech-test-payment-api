using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PaymentAPI.Models
{
    public class ItemVenda
    {
        public int Id { get; set; }
        public string Descricao { get; set; }
        public decimal Preco { get; set; }     
        public int quantidade { get; set; }
        public decimal Total { get; set; }

    }
}