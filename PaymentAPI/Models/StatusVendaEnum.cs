using System.ComponentModel;

namespace PaymentAPI.Models
{
    [DefaultValue(StatusVendaEnum.AguardandoPagamento)]
    public enum StatusVendaEnum
    {
        [Description("Aguardando Pagamento")]
        AguardandoPagamento = 0,
        PagamentoAprovado = 1,
        EnviadoParaTransportadora = 2,
        Entregue = 3,
        Cancelado = 4
    }
}